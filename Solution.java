import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

public class Solution {
    public static void main(String[] args) {
        double[] arr = statisticEvaluation(10, 100, 10);
        // use arr data to draw histograms (use python lib)
    }

    /**
     * get the index gi = g(i) of the left descendant of the index i
     *
     * @param i
     * @return int
     */
    public static int g(int i) {
        // solving a quadratic equation m(m+1)/2 = i + 1 to get positive integer l are level of i
        int l = (int) Math.floor((Math.sqrt(8 * (i + 1)) - 1) / 2);
        // get position of i by calculate number of element from level 0 -> level (l -1)
        // after that get i subtract number of element from level 0 - level (l - 1) to get p are position of i in level l
        int position = i - (l - 1) * (l - 1 + 1) / 2;
        // get number of element from level 0 -> level l
        // because i and g(i) are same position in its level so
        // add number of element from level 0 -> level l with position of i in level l -> index left descendant of i in level (l + 1)
        return l * (l + 1) / 2 + 1 + position;
    }

    /**
     * get the index di = d(i) of the right descendant of the index i
     *
     * @param i
     * @return int
     */
    public static int d(int i) {
        return g(i) + 1;
    }

    /**
     * calculate path from each point to the end use greedy solution (local optimize)
     * in each point if the vale of left is greater than that on the right we will go left and vice versa
     *
     * @param rectangle
     * @return int[]
     */
    public static int[] calculateMaxPathByLocalOptimize(int[] rectangle) {
        int arrLength = rectangle.length;
        // initialize result array
        int[] result = new int[arrLength];
        for (int i = 0; i < arrLength; i++) {
            if (g(i) > arrLength) {
                // i is a leaf, only return value on i
                result[i] = rectangle[i];
            } else {
                // i not a leaf
                int temp = i;
                // initialize result = value on i
                int sum = rectangle[temp];
                // path will stop if go to the leaf
                while (g(temp) < arrLength) {
                    // compare left and right value
                    if (rectangle[g(temp)] >= rectangle[d(temp)]) {
                        temp = g(temp);
                    } else {
                        temp = d(temp);
                    }
                    // add value of left/right to result
                    sum += rectangle[temp];
                }
                result[i] = sum;
            }
        }
        return result;
    }

    /**
     * calculate path from each point to the end use global optimization
     *
     * @param rectangle
     * @return int[]
     */
    public static int[] calculateMaxPathByGlobalOptimize(int[] rectangle) {
        int[] result = Arrays.copyOfRange(rectangle, 0, rectangle.length);
        // get max level of rectangle
        int l = (int) Math.floor((Math.sqrt(8 * result.length) - 1) / 2);
        // we will calculate maximum path from high-level to low-level of rectangle
        // the max value of a index in low-level = max(g(i), d(i))
        for (int i = l - 1; i >= 0; i--) {
            // loop via all element in a level
            for (int j = 0; j <= i; j++) {
                // get index of element in array
                int index = i * (i + 1) / 2 + j;
                result[index] = result[index] + Math.max(result[g(index)], result[d(index)]);
            }
        }
        return result;
    }

    /**
     * print maximum path from index i to n
     *
     * @param M
     * @param T
     * @param i
     * @param n
     */
    public static void acsm(int[] M, int[] T, int i, int n) {
        // initialize path
        List<Integer> path = new ArrayList<>();
        int temp = i;
        // add start point
        path.add(T[i]);
        while (g(temp) < n) {
            // determine go to left or right base on greater than path (calculated by calculateMaxPathByGlobalOptimize function)
            if (M[g(temp)] >= M[d(temp)]) {
                // go left
                path.add(T[g(temp)]);
                temp = g(temp);
            } else {
                // go right
                path.add(T[d(temp)]);
                temp = d(temp);
            }
        }
        // print path
        System.out.println(path.stream().map(Objects::toString).collect(Collectors.joining("->")));
    }

    /**
     * statistic evaluation base on `lMax`, `nRun`, `vMax`
     *
     * @param lMax
     * @param nRun
     * @param vMax
     * @return double[]
     */
    public static double[] statisticEvaluation(int lMax, int nRun, int vMax) {
        double[] D = new double[nRun];
        for (int i = 0; i < nRun; i++) {
            System.out.println(i);
            int m = new Random().nextInt(lMax + 1) + 1;
            int n = m * (m + 1) / 2;
            int[] T = new Random().ints(n, 0, vMax + 1).toArray();
            int g = calculateMaxPathByGlobalOptimize(T)[0];
            int v = calculateMaxPathByLocalOptimize(T)[0];
            D[i] = ((double) (g - v)) / g;
        }
        return D;
    }

    /**
     * test g(i) and d(i) function
     * print out g(i) and d(i) for every point in rectangle with level are `l`
     *
     * @param l
     */
    private static void testGiAndDi(int l) {
        int[] arr = new Random().ints(l, 1, 10).toArray();
        for (int i = 0; i < arr.length; i++) {
            System.out.println(String.format("g(%d)=%d, d(%d)=%d", i, g(i), i, d(i)));
        }
    }
}
